// Type definitions for react-native-swiper 1.5
// Project: https://github.com/leecade/react-native-swiper#readme
// Definitions by: Igor Djugostran <https://github.com/idjugostran>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.1

import * as React from 'react';
import {
	ViewStyle,
	ListViewDataSource
} from 'react-native';

interface Properties extends React.Props<RefreshInfiniteListView> {

	style?: ViewStyle;

	dataSource: ListViewDataSource

	footerHeight?: number

	pullDistance?: number

	initialListSize? :number

	renderEmptyRow?: (data: any) => JSX.Element

	renderRow: (data: any) => JSX.Element

	renderHeaderRefreshIdle?: () => JSX.Element

	renderHeaderWillRefresh?: () => JSX.Element

	renderHeaderRefreshing?: () => JSX.Element

	renderFooterWillInifite?: () => JSX.Element

	renderFooterInifiting?: () => JSX.Element

	renderFooterInifiteLoadedAll?: () => JSX.Element

	onRefresh?: () => void

	onInfinite?: () => void

	loadedAllData?: () => void

	scrollEventThrottle?: number
}


export default class RefreshInfiniteListView extends React.Component<Properties, {}> {

	hideHeader: () => void

	hideFooter: () => void
}

